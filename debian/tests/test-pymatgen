#!/bin/bash
set -efu

arch=$(dpkg-architecture -qDEB_HOST_ARCH)

EXTRA_TEST_FLAGS=""

# some tests are expected to fail
# so list tests to skip in array variable SKIP_TEST_LIST
declare -a SKIP_TEST_LIST

# SeeK-path has not been packaged for debian
# so skip SeeK tests in io/pwmat/test_inputs.py
SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" TestGenKpt TestHighSymmetryPoint)

# io/qchem is partly broken, skip failing tests
# io/qchem input
SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_from_multi_jobs_file test_from_string test_read_molecule test_read_nbo test_read_negative test_read_plots test_read_write_nbo7)
# io/qchem output
SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_all test_custom_smd_write)
# io/qchem sets (test_sets.py all fail)
SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_sets)
# test_rotor_search_rrs of io test_babel.py intermittently times out
SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_rotor_search_rrs)
# times out
SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" BSDOSPlotterTest)
# requires write permissions
SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_cdft_dc_parsing)
# requires internet
SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_get_structure_by_id)

# these io tests fail in chroot
SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_CifWriter test_read_write test_init test_get_site_symmetries)

# fragmenter openbabel test fails, see https://github.com/materialsproject/pymatgen/issues/3078
SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_babel_PC_with_RO_depth_0_vs_depth_10)

# MoleculeMatcherTest.test_fit in test_molecule_matcher.py fails for some reason
SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_fit)

# this test is slow and sometimes times out (Bug#1058662)
SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_rotor_search_wrs)

if [ "$arch" = "arm64" ]; then
    SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_get_parchg "(TestAnharmonicQuasiharmociDebyeApprox and test_gruneisen_parameter)")
fi

if [ "$arch" = "i386" ]; then
    SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_get_parchg test_numerical_eoswrapper "(TestAnharmonicQuasiharmociDebyeApprox and test_gruneisen_parameter)")
fi

if [ "$arch" = "mips64el" ]; then
    SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_pca test_standard test_from_diff_fit test_relax_ase_opt_kwargs test_relax_ase_return_traj)
fi

if [ "$arch" = "ppc64el" ]; then
    SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_get_parchg test_numerical_eoswrapper "(TestAnharmonicQuasiharmociDebyeApprox and test_gruneisen_parameter)")
fi

if [ "$arch" = "riscv64" ]; then
    EXTRA_TEST_FLAGS="${EXTRA_TEST_FLAGS} -v"
    SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_get_parchg)
    SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_relax_ase_opt_kwargs test_relax_ase_return_traj test_rotate_sites)
    SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" "(TestAnharmonicQuasiharmociDebyeApprox and test_gruneisen_parameter)")
    # skip slowest tests to avoid timeout
    SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_xrd test_volume_predictor test_structure test_tensors test_boltztrap test_optics test_outputs test_dos)
fi

if [ "$arch" = "s390x" ]; then
    SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_spillage_from_vasprun test_from_file test_optics test_generate_g_points test_generate_nbmax test_evaluate_wavefunc)
    SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_fft_mesh_advanced test_fft_mesh_basic test_get_parchg test_n2_45210 test_n2_spin test_standard test_write_unks test_consistency)
    SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_read_fe)
    SKIP_TEST_LIST=("${SKIP_TEST_LIST[@]}" test_process_parsed_HESS "(TestAnharmonicQuasiharmociDebyeApprox and test_gruneisen_parameter)")
fi

SKIP_TESTS=""
list_initialised=0
for t in "${SKIP_TEST_LIST[@]}"; do
    if [ ${list_initialised} = 0 ]; then
        SKIP_TESTS=$t
        list_initialised=1
    else
        SKIP_TESTS="${SKIP_TESTS} or $t"
    fi
done
if [ "x${SKIP_TESTS}" != "x" ]; then
    SKIP_TESTS="not ( ${SKIP_TESTS} )"
fi
echo "skipping tests with SKIP_TEST_LIST=${SKIP_TEST_LIST[@]}"

for py in `py3versions -s`; do
  PMG_TEST_FILES_DIR=/usr/share/doc/pymatgen-test-files/examples/files \
    $py -m pytest -v ${EXTRA_TEST_FLAGS} --durations=30 --color=no  -k "${SKIP_TESTS}" tests
done
