Source: pymatgen
Section: python
Priority: optional
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders: Drew Parsons <dparsons@debian.org>
Build-Depends: debhelper-compat (= 13),
 pymatgen-test-files (>= 2025.2.18~) <!nocheck>,
 dh-python,
 dh-sequence-python3,
 cython3,
 pybuild-plugin-pyproject,
 python3-setuptools,
 python3-all-dev,
 python3-pytest,
 python3-frozendict,
 python3-matplotlib (>= 3.8~),
 python3-monty (>= 2025.1.9~),
 python3-networkx (>= 3~),
 python3-numpy (>= 1.25.0~),
 python3-palettable (>= 3.3.3~),
 python3-pandas (>= 2~),
 python3-plotly (>= 5.0.0~),
 python3-pybtex (>= 0.24.0~),
 python3-requests (>= 2.32~),
 python3-ruamel.yaml (>= 0.17.0~),
 python3-scipy (>= 1.13.0~),
 python3-spglib (>= 2.5.0~),
 python3-sympy (>= 1.6.2~),
 python3-tabulate (>= 0.9~),
 python3-tqdm (>= 4.60~),
 python3-uncertainties (>= 3.1.4~),
 python3-joblib (>= 1~),
 python3-ase (>= 3.23.0~),
 python3-bs4 (>= 4.9.1~),
 python3-vtk9 | python3-paraview,
 python3-h5py (>= 3.11.0~),
 python3-netcdf4 (>= 1.6.5~),
 python3-phonopy (>= 2.33.3~),
 python3-openbabel (>= 3.1.1~),
 packmol [!armel !riscv64] <!nocheck>
Build-Depends-Indep:
 dh-sequence-sphinxdoc <!nodoc>,
 sphinx | python3-sphinx | sphinx-common | dh-sequence-sphinxdoc <!nodoc>,
 libjs-jquery <!nodoc>,
 libjs-mathjax <!nodoc>,
 python3-sphinx-rtd-theme <!nodoc>
Standards-Version: 4.7.0
Homepage: https://pymatgen.org/
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debichem-team/pymatgen
Vcs-Git: https://salsa.debian.org/debichem-team/pymatgen.git

Package: python3-pymatgen
Architecture: any
Depends: ${python3:Depends},
 ${misc:Depends},
 ${shlibs:Depends}
Recommends: python3-openbabel,
  python3-ase (>= 3.23.0~),
  python3-vtk9 | python3-paraview
Suggests: python-pymatgen-doc, python3-mp-api (>= 0.27.3~)
Description: Python Materials Genomics for materials analysis
 Pymatgen (Python Materials Genomics) is a robust, open-source Python
 library for materials analysis. These are some of the main features:
 .
 1.Highly flexible classes for the representation of Element, Site,
 Molecule, Structure objects.
 .
 2. Extensive input/output support, including support for VASP
 (http://cms.mpi.univie.ac.at/vasp/), ABINIT (http://www.abinit.org/),
 CIF, Gaussian, XYZ, and many other file formats.
 .
 3. Powerful analysis tools, including generation of phase diagrams,
 Pourbaix diagrams, diffusion analyses, reactions, etc.
 .
 4. Electronic structure analyses, such as density of states and band
 structure.
 .
 5. Integration with the Materials Project REST API, Crystallography
 Open Database.
 .
 This package provides the pymtgen module for Python 3.

Package: python-pymatgen-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: libjs-jquery, libjs-mathjax, ${sphinxdoc:Depends}, ${misc:Depends}
Description: Python Materials Genomics for materials analysis (documentation)
 Pymatgen (Python Materials Genomics) is a robust, open-source Python
 library for materials analysis. These are some of the main features:
 .
 1.Highly flexible classes for the representation of Element, Site,
 Molecule, Structure objects.
 .
 2. Extensive input/output support, including support for VASP
 (http://cms.mpi.univie.ac.at/vasp/), ABINIT (http://www.abinit.org/),
 CIF, Gaussian, XYZ, and many other file formats.
 .
 3. Powerful analysis tools, including generation of phase diagrams,
 Pourbaix diagrams, diffusion analyses, reactions, etc.
 .
 4. Electronic structure analyses, such as density of states and band
 structure.
 .
 5. Integration with the Materials Project REST API, Crystallography
 Open Database.
 .
 This is the documentation package for pymatgen.
